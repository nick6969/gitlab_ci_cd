package main

import "testing"

func Test_plus(t *testing.T) {
	ans := plus(1, 1)
	if ans != 2 {
		t.Errorf("should got 2, but got %d", ans)
	}
}
