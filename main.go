package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "{\"success\":true}")
	})

	log.Println(plus(1, 1))

	_ = http.ListenAndServe(":8080", nil)

}

func plus(lhs, rhs int) int {
	return lhs + rhs
}
